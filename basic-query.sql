-- PRECISION DE LA BASE DE DONNÉES À UTILISER
USE transport_logistique;

-- SELECTIONNER TOUS LES ENTREPOTS
SELECT *
FROM entrepots;

-- SELECTIONNER TOUTES LES EXPEDITIONS
SELECT *
FROM expeditions;

-- SELECTIONNER TOUTES LES EXPEDITIONS LIVRÉE
SELECT *
FROM expeditions
WHERE statut = 'Livrée';

-- SELECTIONNER TOUTES LES EXPEDITIONS EN TRANSIT
SELECT *
FROM expeditions
WHERE statut = 'En transit';
