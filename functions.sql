CREATE FUNCTION get_open_days_between(start_date DATE, end_date DATE) RETURNS INT
BEGIN
    DECLARE open_days INT;
    DECLARE current_test_date DATE;
    SET open_days = 0;
    SET current_test_date = start_date;

    WHILE current_test_date < end_date
        DO
            -- Vérifiez si le jour actuel n'est ni un samedi ni un dimanche
            IF DAYOFWEEK(current_test_date) NOT IN (1, 7) THEN
                SET open_days = open_days + 1;
            END IF;

            SET current_test_date = DATE_ADD(current_test_date, INTERVAL 1 DAY);
        END WHILE;

    RETURN open_days;
END;
