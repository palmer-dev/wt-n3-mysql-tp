-- PRECISION DE LA BASE DE DONNÉES À UTILISER
USE transport_logistique;

-- CRÉEZ UNE VUE QUI AFFICHE LES INFORMATIONS SUIVANTES POUR CHAQUE ENTREPÔT : NOM DE L'ENTREPÔT, ADRESSE COMPLÈTE, NOMBRE D'EXPÉDITIONS ENVOYÉES AU COURS DES 30 DERNIERS JOURS.
CREATE VIEW last_thirty_days_entrepot_export AS
SELECT nom_entrepot, CONCAT(adresse, ', ', ville, ', ', nom_pays) as adresse_complete, COUNT(*) as exp_sent
FROM expeditions
         INNER JOIN transport_logistique.entrepots src on expeditions.id_entrepot_source = src.id
         INNER JOIN pays ON pays_id = pays.id
WHERE date_expedition > DATE_ADD(NOW(), INTERVAL -30 DAY)
GROUP BY src.id;

-- VOIR LA VIEW
SELECT *
FROM last_thirty_days_entrepot_export;


-- CRÉEZ UNE PROCÉDURE STOCKÉE QUI PREND EN ENTRÉE L'ID D'UN ENTREPÔT ET RENVOIE LE NOMBRE TOTAL D'EXPÉDITIONS ENVOYÉES PAR CET ENTREPÔT AU COURS DU DERNIER MOIS.
CREATE PROCEDURE last_month_exp_by_id_entrepot(id_entrepot INT)
BEGIN
    SELECT COUNT(*) as exp_sent
    FROM expeditions
             INNER JOIN transport_logistique.entrepots src on expeditions.id_entrepot_source = src.id
    WHERE date_expedition > DATE_ADD(NOW(), INTERVAL -30 DAY)
      AND src.id = id_entrepot
    GROUP BY src.id;
END;

-- TESTER LA PROCÉDURE
CALL last_month_exp_by_id_entrepot(7);


-- CRÉEZ UNE FONCTION QUI PREND EN ENTRÉE UNE DATE ET RENVOIE LE NOMBRE TOTAL D'EXPÉDITIONS LIVRÉES CE JOUR-LÀ.
CREATE FUNCTION get_delivery_in_day(query_date DATE) RETURNS INT
BEGIN
    DECLARE nb_delivery INT;
    SET nb_delivery = 0;

    SELECT COUNT(*) into nb_delivery FROM expeditions WHERE date_expedition = query_date;

    RETURN nb_delivery;
END;

-- TESTER LA FONCTION
SELECT get_delivery_in_day('2022-11-02')
