-- PRECISION DE LA BASE DE DONNÉES À UTILISER
USE transport_logistique;

-- INSERT DATA IN ENTREPOTS
INSERT INTO entrepots (nom_entrepot, adresse, ville, pays_id)
VALUES ('Marseille Prado', '4 rue des livraisons', 'Marseille', 74),
       ('Berlin', '12 chemin des esperelles', 'Berlin', 56),
       ('Hamburg haffen', '12 germany strasse', 'Hamburg', 56),
       ('Rome', '30bis impasse des chèvres', 'Rome', 107),
       ('London', '2 chemin des as', 'London', 76),
       ('Pekin', '1 china road', 'Pekin', 48),
       ('Taiwan', '10 tokyo city', 'Tokyo', 113),
       ('Taipei', '32 tapei road', 'Taipei', 225),
       ('Lyon St Exupery', '4 routes de Prague', 'Lyon', 74);

-- INSERT DATA IN EXPEDITION
INSERT INTO expeditions (date_expedition, date_reception, id_entrepot_source, id_entrepot_destination, poids, statut)
VALUES ('2023-01-01', null, 1, 2, 23.1, 'En transit'),
       ('2023-03-01', null, 3, 2, 103.1, 'En transit'),
       ('2023-05-01', '2023-05-10', 4, 5, 1000.1, 'Livrée'),
       ('2023-05-10', null, 2, 4, 3213.42, 'En transit'),
       ('2023-06-01', '2023-06-04', 3, 4, 2003.21, 'Livrée'),
       ('2023-07-01', null, 1, 5, 223.25, 'En transit'),
       ('2023-10-01', null, 4, 1, 3153.72, 'En transit'),
       ('2023-11-01', '2023-11-03', 5, 2, 5123.63, 'Livrée'),
       ('2023-11-03', null, 7, 1, 5123.63, 'En transit'),
       ('2023-11-02', null, 4, 6, 5123.63, 'En transit'),
       ('2022-11-02', '2022-11-10', 2, 7, 3243.3, 'Livrée'),
       ('2022-11-12', '2022-11-14', 5, 6, 3543.3, 'En transit'),
       ('2022-11-21', '2022-12-10', 8, 3, 233.3, 'Livrée'),
       ('2022-11-24', '2022-12-24', 6, 4, 463.3, 'Livrée'),
       ('2023-11-03', null, 4, 5, 2423.5, 'En transit'),
       ('2023-10-03', null, 4, 5, 2423.5, 'En transit'),
       ('2023-09-03', null, 4, 5, 2423.5, 'En transit'),
       ('2023-09-20', null, 1, 5, 2000.0, 'En transit'),
       ('2023-09-20', null, 4, 5, 2423.5, 'En transit');
