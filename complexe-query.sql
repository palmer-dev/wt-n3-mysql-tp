-- PRECISION DE LA BASE DE DONNÉES À UTILISER
USE transport_logistique;

-- AFFICHEZ LES EXPÉDITIONS EN TRANSIT QUI ONT ÉTÉ INITIÉES PAR UN ENTREPÔT SITUÉ EN EUROPE ET À DESTINATION D'UN ENTREPÔT SITUÉ EN ASIE.
SELECT expeditions.*, src.nom_entrepot, des.nom_entrepot
FROM expeditions
         INNER JOIN transport_logistique.entrepots src on expeditions.id_entrepot_source = src.id
         INNER JOIN transport_logistique.entrepots des on expeditions.id_entrepot_destination = des.id
         INNER JOIN transport_logistique.pays psrc on src.pays_id = psrc.id
         INNER JOIN transport_logistique.pays pdes on des.pays_id = pdes.id
WHERE psrc.continent = 'Europe'
  AND pdes.continent = 'Asia'
  AND statut = 'En transit';

-- AFFICHEZ LES ENTREPÔTS QUI ONT ENVOYÉ DES EXPÉDITIONS À DESTINATION D'UN ENTREPÔT SITUÉ DANS LE MÊME PAYS.
SELECT expeditions.*, src.nom_entrepot, des.nom_entrepot
FROM expeditions
         INNER JOIN transport_logistique.entrepots src on expeditions.id_entrepot_source = src.id
         INNER JOIN transport_logistique.entrepots des on expeditions.id_entrepot_destination = des.id
WHERE src.pays_id = des.pays_id;

-- AFFICHEZ LES ENTREPÔTS QUI ONT ENVOYÉ DES EXPÉDITIONS À DESTINATION D'UN ENTREPÔT SITUÉ DANS UN PAYS DIFFÉRENT.
SELECT expeditions.*, src.nom_entrepot, des.nom_entrepot
FROM expeditions
         INNER JOIN transport_logistique.entrepots src on expeditions.id_entrepot_source = src.id
         INNER JOIN transport_logistique.entrepots des on expeditions.id_entrepot_destination = des.id
WHERE src.pays_id <> des.pays_id;

-- AFFICHEZ LES EXPÉDITIONS EN TRANSIT QUI ONT ÉTÉ INITIÉES PAR UN ENTREPÔT SITUÉ DANS UN PAYS DONT LE NOM COMMENCE PAR LA LETTRE "F" ET QUI PÈSENT PLUS DE 500 KG.
SELECT expeditions.*, src.nom_entrepot
FROM expeditions
         INNER JOIN transport_logistique.entrepots src on expeditions.id_entrepot_source = src.id
         INNER JOIN transport_logistique.pays psrc on src.pays_id = psrc.id
WHERE SUBSTRING(psrc.nom_pays, 1, 1) = 'F' AND poids > 500;

-- AFFICHEZ LE NOMBRE TOTAL D'EXPÉDITIONS POUR CHAQUE COMBINAISON DE PAYS D'ORIGINE ET DE DESTINATION.
SELECT CONCAT(src.nom_entrepot, ' - ', des.nom_entrepot) as noms_entrepots, COUNT(expeditions.id) as nb_expedition
FROM expeditions
         INNER JOIN transport_logistique.entrepots src on expeditions.id_entrepot_source = src.id
         INNER JOIN transport_logistique.entrepots des on expeditions.id_entrepot_destination = des.id
GROUP BY CONCAT(src.nom_entrepot, ' - ', des.nom_entrepot);

-- AFFICHEZ LES ENTREPÔTS QUI ONT ENVOYÉ DES EXPÉDITIONS AU COURS DES 30 DERNIERS JOURS ET DONT LE POIDS TOTAL DES EXPÉDITIONS EST SUPÉRIEUR À 1000 KG.
-- Création d'une view pour pré-calculer les données de poids sur la table
CREATE VIEW weight_exp_last_thirty_day AS
SELECT src.*, SUM(expeditions.poids) as total_weight
FROM expeditions
         INNER JOIN transport_logistique.entrepots src on expeditions.id_entrepot_source = src.id
WHERE date_expedition > DATE_ADD(NOW(), INTERVAL -30 DAY)
GROUP BY src.id;

-- Utilisation de la view avec les données pré-calculées de poids des expéditions
SELECT *
FROM weight_exp_last_thirty_day
WHERE total_weight > 1000;

-- AFFICHEZ LES EXPÉDITIONS QUI ONT ÉTÉ LIVRÉES AVEC UN RETARD DE PLUS DE 2 JOURS OUVRABLES.
-- Voir l'ajout de la colonne : creation.sql:37
SELECT expeditions.date_eta,
       expeditions.date_reception,
       DATEDIFF(expeditions.date_reception, expeditions.date_eta) as datetiff,
       get_open_days_between(expeditions.date_eta, expeditions.date_reception) as late_days
FROM expeditions
WHERE get_open_days_between(expeditions.date_eta, expeditions.date_reception) > 2;

-- AFFICHEZ LE NOMBRE TOTAL D'EXPÉDITIONS POUR CHAQUE JOUR DU MOIS EN COURS, TRIÉ PAR ORDRE DÉCROISSANT.
SELECT date_expedition, COUNT(id) as nb_exp_in_day
FROM expeditions
WHERE date_expedition >= DATE(DATE_SUB(NOW(),INTERVAL DAYOFMONTH(NOW())-1 DAY))
GROUP BY date_expedition
ORDER BY COUNT(id) DESC;
