-- CRÉATION DE LA BASE DE DONNÉES transport_logistique
CREATE DATABASE transport_logistique;

-- PRECISION DE LA BASE DE DONNÉES À UTILISER
USE transport_logistique;

-- AJOUT D'UNE TABLE DU MONDE AVEC LES PAYS ET CONTINENTS
CREATE TABLE pays
(
    id        INT AUTO_INCREMENT PRIMARY KEY,
    nom_pays  VARCHAR(255) NOT NULL,
    continent VARCHAR(255) NOT NULL
);


-- CRÉATION DE LA TABLE ENTREPOTS
CREATE TABLE entrepots
(
    id           INT PRIMARY KEY AUTO_INCREMENT,
    nom_entrepot VARCHAR(100) NOT NULL,
    adresse      VARCHAR(100) NOT NULL,
    ville        VARCHAR(100) NOT NULL,
    pays         VARCHAR(100) NOT NULL
);

-- MISE A JOUR DE LA TABLE ENTREPOTS AVEC LES PAYS
ALTER TABLE entrepots
    DROP pays,
    ADD pays_id INT NULL,
    ADD FOREIGN KEY (pays_id) REFERENCES pays (id);


-- CRÉATION DE LA TABLE EXPEDITIONS
CREATE TABLE expeditions
(
    id                      INT PRIMARY KEY AUTO_INCREMENT,
    date_expedition         DATE NOT NULL,
    id_entrepot_source      INT  NOT NULL,
    id_entrepot_destination INT  NOT NULL,
    poids                   FLOAT,
    statut                  VARCHAR(40),
    FOREIGN KEY (id_entrepot_source) REFERENCES entrepots (id),
    FOREIGN KEY (id_entrepot_destination) REFERENCES entrepots (id)
);


-- AJOUT D'UNE COLONNE DATE DE RECEPTION POUR LA QUESTION : Affichez les expéditions qui ont été livrées dans un délai de moins de 5 jours ouvrables.
ALTER TABLE expeditions
    ADD date_reception DATE NULL;


-- AJOUT D'UNE COLONNE date_eta POUR LA QUESTION : Affichez les expéditions qui ont été livrées avec un retard de plus de 2 jours ouvrables.
ALTER TABLE expeditions
    ADD date_eta DATE NULL;

-- Ajout d'un trigger pour mettre une valeur par défaut calculée.
CREATE TRIGGER chk_expedition_date_eta
    BEFORE INSERT
    ON `expeditions`
    FOR EACH ROW
BEGIN
    IF (NEW.date_eta is null) THEN
        SET NEW.date_eta = DATE_ADD(NEW.date_expedition, INTERVAL 5 DAY);
    END IF;
END;
