# Rapport du TP : Base de données de transport

Ce TP a été réalisé à l'aide du logiciel DataGrip et d'une base de données MySQL (MySQL Server 5.7.42-1.el7).

## Création de la base de données

Toutes les commandes de cette partie se trouvent dans le fichier [creation.sql](creation.sql)

```sql
CREATE
    DATABASE transport_logistique;
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/01.png)
___

## Création des tables de la base de données

Quelques modifications sur les tables ont pu être apportée pour répondre aux différents besoins des questions du TP.
C'est
pour cela qu'il y a aura dans le document des requêtes marquées comme <span style="color: red">#supplémentaires</span>.

### Création de la table _pays_ <span style="color: red">#supplémentaire</span>

Cette table n'était pas demandée dans le TP, mais pour certaines questions, les informations qu'elles apportent sont
utiles. (L'information du continent nous intéresse plus particulièrement pour une des questions un peu plus loin dans ce
rapport).

```sql 
CREATE TABLE pays
(
    id        INT AUTO_INCREMENT PRIMARY KEY,
    nom_pays  VARCHAR(255) NOT NULL,
    continent VARCHAR(255) NOT NULL
);
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/02.png)
___

### Création de la table _entrepots_

Aucune modification n'a été apporté pour la création initiale de la table _entrepots_. Les colonnes sont celles
demandées dans la consigne.

```sql 
CREATE TABLE entrepots
(
    id           INT PRIMARY KEY AUTO_INCREMENT,
    nom_entrepot VARCHAR(100) NOT NULL,
    adresse      VARCHAR(100) NOT NULL,
    ville        VARCHAR(100) NOT NULL,
    pays         VARCHAR(100) NOT NULL
);
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/03.png)
___

### Modification de la table _entrepots_ <span style="color: red">#supplémentaire</span>

Ici on modifie la façon dont est stocké le pays relatif à l'entrepot pour obtenir plus d'information sans changer de
façon trop importante la table. On ajoute alors une clé étrangère.

```sql 
ALTER TABLE entrepots
    DROP
        pays,
    ADD pays_id INT NULL,
    ADD FOREIGN KEY (pays_id) REFERENCES pays (id);
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/04.png)
___

### Création de la table _expeditions_

Aucune modification n'a été apporté pour la création initiale de la table _expeditions_. Les colonnes sont celles
demandées dans la consigne.

```sql 
CREATE TABLE expeditions
(
    id                      INT PRIMARY KEY AUTO_INCREMENT,
    date_expedition         DATE NOT NULL,
    id_entrepot_source      INT  NOT NULL,
    id_entrepot_destination INT  NOT NULL,
    poids                   FLOAT,
    statut                  VARCHAR(40),
    FOREIGN KEY (id_entrepot_source) REFERENCES entrepots (id),
    FOREIGN KEY (id_entrepot_destination) REFERENCES entrepots (id)
);
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/05.png)
___

### Modification de la table _expeditions_ <span style="color: red">#supplémentaire</span>

Ajout d'une colonne de date de réception pour permettre de savoir quand ont été reçu les expéditions. (Utile pour la
question des commandes reçues les 30 derniers jours)

```sql
ALTER TABLE expeditions
    ADD date_reception DATE NULL;
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/06.png)
___

Ajout d'une colonne de date estimée d'arrivée de l'expédition. Cette colonne permet de calculer le nombre de jours de
différence entre ce qui était prévu est ce qui a été réellement fait.

```sql
ALTER TABLE expeditions
    ADD date_eta DATE NULL;
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/07.png)
___

Ajout d'une automatisation pour ajouter une date estimée d'arrivé automatiquement si aucune n'est renseignée. Celle-ci
est définie par défaut 5 jours après la date d'expédition.

```sql
CREATE TRIGGER chk_expedition_date_eta
    BEFORE INSERT
    ON `expeditions`
    FOR EACH ROW
BEGIN
    IF (NEW.date_eta is null) THEN
        SET NEW.date_eta = DATE_ADD(NEW.date_expedition, INTERVAL 5 DAY);
    END IF;
END;
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/08.png)
___

## Ajout de données

Toutes les commandes de cette partie se trouvent dans les fichiers [insert-pays.sql](insert-pays.sql)
et [insert.sql](insert.sql)

### Insertion de données dans la table _pays_ <span style="color: red">#supplémentaire</span>

On commence par importer les données dans la table pays, car elles sont nécessaires pour la création d'entrepôts. Les
données de cette table ont été récupérée depuis un git qui rassemble les données des pays et
continents [alyssaq/countryes-continent.sql](https://gist.github.com/alyssaq/4edb5f660ff3507d3e0e).
La requête complète (246 lignes) se trouve dans le fichier [insert-pays.sql](insert-pays.sql).

```sql
INSERT INTO pays(nom_pays, continent)
VALUES ('Andorra', 'EU'),
       ('United Arab Emirates', 'AS'),
       ('Afghanistan', 'AS'),
       ('Antigua and Barbuda', 'NA');
...
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/09.png)
___

### Mise à jour des données de la table

_pays_ pour correspondre aux besoins du TP <span style="color: red">#supplémentaire</span>

On met à jour les données pour transformer le code à deux lettres du continent vers la version complète du nom du
continent (en anglais ici choix personnel).

```sql 
UPDATE pays
SET continent =
        CASE
            WHEN continent = 'EU' THEN 'Europe'
            WHEN continent = 'AS' THEN 'Asia'
            WHEN continent = 'NA' THEN 'North America'
            WHEN continent = 'SA' THEN 'South America'
            WHEN continent = 'AF' THEN 'Africa'
            WHEN continent = 'OC' THEN 'Oceania'
            WHEN continent = 'AN' THEN 'Antarctica'
            ELSE continent
            END;
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/10.png)
___

### Insertion de données dans la table _entrepots_

On insert les données selon le format **APRÈS MISE À JOUR** qui change la colonne _pays_ en _pays_id_  de la table
_entrepots_

```sql
INSERT INTO entrepots (nom_entrepot, adresse, ville, pays_id)
VALUES ('Marseille Prado', '4 rue des livraisons', 'Marseille', 74),
       ('Berlin', '12 chemin des esperelles', 'Berlin', 56),
       ('Hamburg haffen', '12 germany strasse', 'Hamburg', 56),
       ('Rome', '30bis impasse des chèvres', 'Rome', 107),
       ('London', '2 chemin des as', 'London', 76),
       ('Pekin', '1 china road', 'Pekin', 48),
       ('Taiwan', '10 tokyo city', 'Tokyo', 113),
       ('Taipei', '32 tapei road', 'Taipei', 225),
       ('Lyon St Exupery', '4 routes de Prague', 'Lyon', 74);
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/11.png)
___

### Insertion de données dans la table _expeditions_

On insert les données selon le format **APRÈS MISE À JOUR** qui ajoute les colonnes _date_eta_ et _date_reception_ de la
table _expeditions_

```sql
INSERT INTO expeditions (date_expedition, date_reception, id_entrepot_source, id_entrepot_destination, poids, statut)
VALUES ('2023-01-01', null, 1, 2, 23.1, 'En transit'),
       ('2023-03-01', null, 3, 2, 103.1, 'En transit'),
       ('2023-05-01', '2023-05-10', 4, 5, 1000.1, 'Livrée'),
       ('2023-05-10', null, 2, 4, 3213.42, 'En transit'),
       ('2023-06-01', '2023-06-04', 3, 4, 2003.21, 'Livrée'),
       ('2023-07-01', null, 1, 5, 223.25, 'En transit'),
       ('2023-10-01', null, 4, 1, 3153.72, 'En transit'),
       ('2023-11-01', '2023-11-03', 5, 2, 5123.63, 'Livrée'),
       ('2023-11-03', null, 7, 1, 5123.63, 'En transit'),
       ('2023-11-02', null, 4, 6, 5123.63, 'En transit'),
       ('2022-11-02', '2022-11-10', 2, 7, 3243.3, 'Livrée'),
       ('2022-11-12', '2022-11-14', 5, 6, 3543.3, 'En transit'),
       ('2022-11-21', '2022-12-10', 8, 3, 233.3, 'Livrée'),
       ('2022-11-24', '2022-12-24', 6, 4, 463.3, 'Livrée'),
       ('2023-11-03', null, 4, 5, 2423.5, 'En transit'),
       ('2023-10-03', null, 4, 5, 2423.5, 'En transit'),
       ('2023-09-03', null, 4, 5, 2423.5, 'En transit'),
       ('2023-09-20', null, 1, 5, 2000.0, 'En transit'),
       ('2023-09-20', null, 4, 5, 2423.5, 'En transit');
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/12.png)
___

## Requêtes de base

Toutes les commandes de cette partie se trouvent dans le fichier [basic-query.sql](basic-query.sql).

### Afficher tous les entrepôts

```sql
SELECT *
FROM entrepots;
```

*Tableau de données de sortie de la requête SQL*

| id | nom\_entrepot   | adresse                   | ville     | pays\_id |
|:---|:----------------|:--------------------------|:----------|:---------|
| 1  | Marseille Prado | 4 rue des livraisons      | Marseille | 74       |
| 2  | Berlin          | 12 chemin des esperelles  | Berlin    | 56       |
| 3  | Hamburg haffen  | 12 germany strasse        | Hamburg   | 56       |
| 4  | Rome            | 30bis impasse des chèvres | Rome      | 107      |
| 5  | London          | 2 chemin des as           | London    | 76       |
| 6  | Pekin           | 1 china road              | Pekin     | 48       |
| 7  | Taiwan          | 10 tokyo city             | Tokyo     | 113      |
| 8  | Taipei          | 32 tapei road             | Taipei    | 225      |
| 9  | Lyon St Exupery | 4 routes de Prague        | Lyon      | 74       |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/13.png)
___

### Afficher toutes les expéditions

```sql
SELECT *
FROM expeditions;
```

*Tableau de données de sortie de la requête SQL*

| id | date\_expedition | id\_entrepot\_source | id\_entrepot\_destination | poids   | statut     | date\_reception | date\_eta  |
|:---|:-----------------|:---------------------|:--------------------------|:--------|:-----------|:----------------|:-----------|
| 1  | 2023-01-01       | 1                    | 2                         | 23.1    | En transit | null            | 2023-01-06 |
| 2  | 2023-03-01       | 3                    | 2                         | 103.1   | En transit | null            | 2023-03-06 |
| 3  | 2023-05-01       | 4                    | 5                         | 1000.1  | Livrée     | 2023-05-10      | 2023-05-06 |
| 4  | 2023-05-10       | 2                    | 4                         | 3213.42 | En transit | null            | 2023-05-15 |
| 5  | 2023-06-01       | 3                    | 4                         | 2003.21 | Livrée     | 2023-06-04      | 2023-06-06 |
| 6  | 2023-07-01       | 1                    | 5                         | 223.25  | En transit | null            | 2023-07-06 |
| 7  | 2023-10-01       | 4                    | 1                         | 3153.72 | En transit | null            | 2023-10-06 |
| 8  | 2023-11-01       | 5                    | 2                         | 5123.63 | Livrée     | 2023-11-03      | 2023-11-06 |
| 9  | 2023-11-03       | 7                    | 1                         | 5123.63 | En transit | null            | 2023-11-08 |
| 10 | 2023-11-02       | 4                    | 6                         | 5123.63 | En transit | null            | 2023-11-07 |
| 11 | 2022-11-02       | 2                    | 7                         | 3243.3  | Livrée     | 2022-11-10      | 2022-11-07 |
| 12 | 2022-11-12       | 5                    | 6                         | 3543.3  | En transit | 2022-11-14      | 2022-11-17 |
| 13 | 2022-11-21       | 8                    | 3                         | 233.3   | Livrée     | 2022-12-10      | 2022-11-26 |
| 14 | 2022-11-24       | 6                    | 4                         | 463.3   | Livrée     | 2022-12-24      | 2022-11-29 |
| 15 | 2023-11-03       | 4                    | 5                         | 2423.5  | En transit | null            | 2023-11-08 |
| 16 | 2023-10-03       | 4                    | 5                         | 2423.5  | En transit | null            | 2023-10-08 |
| 17 | 2023-09-03       | 4                    | 5                         | 2423.5  | En transit | null            | 2023-09-08 |
| 18 | 2023-09-20       | 4                    | 5                         | 2423.5  | En transit | null            | 2023-09-25 |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/14.png)
___

### Afficher toutes les expéditions en transit

```sql
SELECT *
FROM expeditions
WHERE statut = 'En transit';
```

*Tableau de données de sortie de la requête SQL*

| id | date\_expedition | id\_entrepot\_source | id\_entrepot\_destination | poids   | statut     | date\_reception | date\_eta  |
|:---|:-----------------|:---------------------|:--------------------------|:--------|:-----------|:----------------|:-----------|
| 1  | 2023-01-01       | 1                    | 2                         | 23.1    | En transit | null            | 2023-01-06 |
| 2  | 2023-03-01       | 3                    | 2                         | 103.1   | En transit | null            | 2023-03-06 |
| 4  | 2023-05-10       | 2                    | 4                         | 3213.42 | En transit | null            | 2023-05-15 |
| 6  | 2023-07-01       | 1                    | 5                         | 223.25  | En transit | null            | 2023-07-06 |
| 7  | 2023-10-01       | 4                    | 1                         | 3153.72 | En transit | null            | 2023-10-06 |
| 9  | 2023-11-03       | 7                    | 1                         | 5123.63 | En transit | null            | 2023-11-08 |
| 10 | 2023-11-02       | 4                    | 6                         | 5123.63 | En transit | null            | 2023-11-07 |
| 12 | 2022-11-12       | 5                    | 6                         | 3543.3  | En transit | 2022-11-14      | 2022-11-17 |
| 15 | 2023-11-03       | 4                    | 5                         | 2423.5  | En transit | null            | 2023-11-08 |
| 16 | 2023-10-03       | 4                    | 5                         | 2423.5  | En transit | null            | 2023-10-08 |
| 17 | 2023-09-03       | 4                    | 5                         | 2423.5  | En transit | null            | 2023-09-08 |
| 18 | 2023-09-20       | 4                    | 5                         | 2423.5  | En transit | null            | 2023-09-25 |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/15.png)

### Afficher toutes les expéditions livrées

```sql
SELECT *
FROM expeditions
WHERE statut = 'Livrée';
```

*Tableau de données de sortie de la requête SQL*

| id | date\_expedition | id\_entrepot\_source | id\_entrepot\_destination | poids   | statut | date\_reception | date\_eta  |
|:---|:-----------------|:---------------------|:--------------------------|:--------|:-------|:----------------|:-----------|
| 3  | 2023-05-01       | 4                    | 5                         | 1000.1  | Livrée | 2023-05-10      | 2023-05-06 |
| 5  | 2023-06-01       | 3                    | 4                         | 2003.21 | Livrée | 2023-06-04      | 2023-06-06 |
| 8  | 2023-11-01       | 5                    | 2                         | 5123.63 | Livrée | 2023-11-03      | 2023-11-06 |
| 11 | 2022-11-02       | 2                    | 7                         | 3243.3  | Livrée | 2022-11-10      | 2022-11-07 |
| 13 | 2022-11-21       | 8                    | 3                         | 233.3   | Livrée | 2022-12-10      | 2022-11-26 |
| 14 | 2022-11-24       | 6                    | 4                         | 463.3   | Livrée | 2022-12-24      | 2022-11-29 |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/16.png)

## Requêtes avancées

Toutes les commandes de cette partie se trouvent dans le fichier [advanced-query.sql](advanced-query.sql).

### Afficher les entrepôts qui ont envoyé au moins une expédition en transit.

On joint la table _expeditions_ ici pour permettre l'accès au status des expéditions.

```sql
SELECT entrepots.*
FROM entrepots
         INNER JOIN transport_logistique.expeditions e on entrepots.id = e.id_entrepot_source
WHERE statut = 'En transit'
GROUP BY entrepots.id;
```

*Tableau de données de sortie de la requête SQL*

| id | nom\_entrepot   | adresse                   | ville     | pays\_id |
|:---|:----------------|:--------------------------|:----------|:---------|
| 1  | Marseille Prado | 4 rue des livraisons      | Marseille | 74       |
| 2  | Berlin          | 12 chemin des esperelles  | Berlin    | 56       |
| 3  | Hamburg haffen  | 12 germany strasse        | Hamburg   | 56       |
| 4  | Rome            | 30bis impasse des chèvres | Rome      | 107      |
| 5  | London          | 2 chemin des as           | London    | 76       |
| 7  | Taiwan          | 10 tokyo city             | Tokyo     | 113      |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/17.png)
___

### Affichez les entrepôts qui ont reçu au moins une expédition en transit.

On joint la table _expeditions_ ici pour permettre l'accès au status des expéditions.

```sql
SELECT entrepots.*
FROM entrepots
         INNER JOIN transport_logistique.expeditions e on entrepots.id = e.id_entrepot_destination
WHERE statut = 'En transit'
GROUP BY entrepots.id;
```

*Tableau de données de sortie de la requête SQL*

| id | nom\_entrepot   | adresse                   | ville     | pays\_id |
|:---|:----------------|:--------------------------|:----------|:---------|
| 1  | Marseille Prado | 4 rue des livraisons      | Marseille | 74       |
| 2  | Berlin          | 12 chemin des esperelles  | Berlin    | 56       |
| 4  | Rome            | 30bis impasse des chèvres | Rome      | 107      |
| 5  | London          | 2 chemin des as           | London    | 76       |
| 6  | Pekin           | 1 china road              | Pekin     | 48       |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/18.png)
___

### Affichez les expéditions qui ont un poids supérieur à 100 kg et qui sont en transit.

On joint la table _expeditions_ ici pour permettre l'accès au status et la poids des expéditions.

```sql
SELECT entrepots.*
FROM entrepots
         INNER JOIN transport_logistique.expeditions e on entrepots.id = e.id_entrepot_destination
WHERE statut = 'En transit'
  AND poids > 100
GROUP BY entrepots.id;
```

*Tableau de données de sortie de la requête SQL*

| id | nom\_entrepot   | adresse                   | ville     | pays\_id |
|:---|:----------------|:--------------------------|:----------|:---------|
| 1  | Marseille Prado | 4 rue des livraisons      | Marseille | 74       |
| 2  | Berlin          | 12 chemin des esperelles  | Berlin    | 56       |
| 4  | Rome            | 30bis impasse des chèvres | Rome      | 107      |
| 5  | London          | 2 chemin des as           | London    | 76       |
| 6  | Pekin           | 1 china road              | Pekin     | 48       |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/19.png)
___

### Affichez le nombre d'expéditions envoyées par chaque entrepôt.

On joint la table _expeditions_ ici pour permettre l'accès au nombre d'expéditions.

```sql
SELECT entrepots.*, COUNT(*) as nb_expedition
FROM entrepots
         INNER JOIN transport_logistique.expeditions e on entrepots.id = e.id_entrepot_source
GROUP BY entrepots.id;
```

*Tableau de données de sortie de la requête SQL*

| id | nom\_entrepot   | adresse                   | ville     | pays\_id | nb\_expedition |
|:---|:----------------|:--------------------------|:----------|:---------|:---------------|
| 1  | Marseille Prado | 4 rue des livraisons      | Marseille | 74       | 2              |
| 2  | Berlin          | 12 chemin des esperelles  | Berlin    | 56       | 2              |
| 3  | Hamburg haffen  | 12 germany strasse        | Hamburg   | 56       | 2              |
| 4  | Rome            | 30bis impasse des chèvres | Rome      | 107      | 7              |
| 5  | London          | 2 chemin des as           | London    | 76       | 2              |
| 6  | Pekin           | 1 china road              | Pekin     | 48       | 1              |
| 7  | Taiwan          | 10 tokyo city             | Tokyo     | 113      | 1              |
| 8  | Taipei          | 32 tapei road             | Taipei    | 225      | 1              |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/20.png)
___

### Affichez le nombre total d'expéditions en transit.

```sql
SELECT COUNT(*) as nb_expeditions_transit
FROM expeditions
WHERE statut = 'En transit';
```

*Tableau de données de sortie de la requête SQL*

| nb\_expeditions\_transit |
|:-------------------------|
| 12                       |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/21.png)
___

### Affichez le nombre total d'expéditions livrées.

```sql
SELECT COUNT(*) as nb_expeditions_livrees
FROM expeditions
WHERE statut = 'Livrée';
```

*Tableau de données de sortie de la requête SQL*

| nb\_expeditions\_livrees |
|:-------------------------|
| 6                        |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/22.png)
___

### Affichez le nombre total d'expéditions pour chaque mois de l'année en cours.

Cette requête n'affiche que la liste des mois dans lesquels il y a eu des expéditions. Si aucune expédition n'a été
réalisée durant le mois alors, il n'est pas affiché.

```sql
SELECT MONTH(date_expedition) as month_expedition, COUNT(*) as nb_expeditions
FROM expeditions
WHERE YEAR(date_expedition) = YEAR(NOW())
GROUP BY MONTH(date_expedition);
```

*Tableau de données de sortie de la requête SQL*

| month\_expedition | nb\_expeditions |
|:------------------|:----------------|
| 1                 | 1               |
| 3                 | 1               |
| 5                 | 2               |
| 6                 | 1               |
| 7                 | 1               |
| 9                 | 2               |
| 10                | 2               |
| 11                | 4               |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/23.png)
___

### Affichez les entrepôts qui ont envoyé des expéditions au cours des 30 derniers jours.

On joint la table _expeditions_ ici pour permettre l'accès à la date d'expédition.

```sql
SELECT etr.*
FROM expeditions
         INNER JOIN transport_logistique.entrepots etr on expeditions.id_entrepot_source = etr.id
WHERE DATEDIFF(NOW(), date_expedition) < 30
GROUP BY etr.id;
```

*Tableau de données de sortie de la requête SQL*

| id | nom\_entrepot | adresse                   | ville  | pays\_id |
|:---|:--------------|:--------------------------|:-------|:---------|
| 4  | Rome          | 30bis impasse des chèvres | Rome   | 107      |
| 5  | London        | 2 chemin des as           | London | 76       |
| 7  | Taiwan        | 10 tokyo city             | Tokyo  | 113      |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/24.png)
___

### Affichez les entrepôts qui ont reçu des expéditions au cours des 30 derniers jours.

On joint la table _expeditions_ ici pour permettre l'accès à la date de réception.

```sql
SELECT etr.*
FROM expeditions
         INNER JOIN transport_logistique.entrepots etr on expeditions.id_entrepot_source = etr.id
WHERE DATEDIFF(NOW(), date_reception) < 30
GROUP BY etr.id;
```

*Tableau de données de sortie de la requête SQL*

| id | nom\_entrepot | adresse         | ville  | pays\_id |
|:---|:--------------|:----------------|:-------|:---------|
| 5  | London        | 2 chemin des as | London | 76       |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/25.png)
___

### Affichez les expéditions qui ont été livrées dans un délai de moins de 5 jours ouvrables.

```sql
SELECT expeditions.*
FROM expeditions
WHERE DATEDIFF(date_reception, date_expedition) < 5;
```

*Tableau de données de sortie de la requête SQL*

| id | date\_expedition | id\_entrepot\_source | id\_entrepot\_destination | poids   | statut     | date\_reception | date\_eta  |
|:---|:-----------------|:---------------------|:--------------------------|:--------|:-----------|:----------------|:-----------|
| 5  | 2023-06-01       | 3                    | 4                         | 2003.21 | Livrée     | 2023-06-04      | 2023-06-06 |
| 8  | 2023-11-01       | 5                    | 2                         | 5123.63 | Livrée     | 2023-11-03      | 2023-11-06 |
| 12 | 2022-11-12       | 5                    | 6                         | 3543.3  | En transit | 2022-11-14      | 2022-11-17 |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/26.png)
___

## Requêtes complèxes

Toutes les commandes de cette partie se trouvent dans le fichier [complexe-query.sql](complexe-query.sql).

### Affichez les expéditions en transit qui ont été initiées par un entrepôt situé en Europe et à destination d'un entrepôt situé en Asie.

On joint plusieurs tables, _entrepots_ pour récupérer l'id du pays dans lequel se trouve l'entrepôt et pouvoir joindre
le pays dans lequel se trouve le continent sur lequel se trouve l'entrepôt. Et on doit faire deux fois cette double
jointure pour avoir le continent de l'entrepôt de départ et de destination.

```sql
SELECT expeditions.*, src.nom_entrepot, des.nom_entrepot
FROM expeditions
         INNER JOIN transport_logistique.entrepots src on expeditions.id_entrepot_source = src.id
         INNER JOIN transport_logistique.entrepots des on expeditions.id_entrepot_destination = des.id
         INNER JOIN transport_logistique.pays psrc on src.pays_id = psrc.id
         INNER JOIN transport_logistique.pays pdes on des.pays_id = pdes.id
WHERE psrc.continent = 'Europe'
  AND pdes.continent = 'Asia'
  AND statut = 'En transit';
```

*Tableau de données de sortie de la requête SQL*

| id | date\_expedition | id\_entrepot\_source | id\_entrepot\_destination | poids   | statut     | date\_reception | date\_eta  | nom\_entrepot | nom\_entrepot |
|:---|:-----------------|:---------------------|:--------------------------|:--------|:-----------|:----------------|:-----------|:--------------|:--------------|
| 4  | 2023-05-10       | 2                    | 4                         | 3213.42 | En transit | null            | 2023-05-15 | Berlin        | Rome          |
| 12 | 2022-11-12       | 5                    | 6                         | 3543.3  | En transit | 2022-11-14      | 2022-11-17 | London        | Pekin         |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/27.png)
___

### Affichez les entrepôts qui ont envoyé des expéditions à destination d'un entrepôt situé dans le même pays.

On joint la table _entrepots_ ici pour permettre l'accès au nom des entrepots de départ et des déstination.

```sql
SELECT expeditions.*, src.nom_entrepot, des.nom_entrepot
FROM expeditions
         INNER JOIN transport_logistique.entrepots src on expeditions.id_entrepot_source = src.id
         INNER JOIN transport_logistique.entrepots des on expeditions.id_entrepot_destination = des.id
WHERE src.pays_id = des.pays_id;
```

*Tableau de données de sortie de la requête SQL*

| id | date\_expedition | id\_entrepot\_source | id\_entrepot\_destination | poids | statut     | date\_reception | date\_eta  | nom\_entrepot  | nom\_entrepot |
|:---|:-----------------|:---------------------|:--------------------------|:------|:-----------|:----------------|:-----------|:---------------|:--------------|
| 2  | 2023-03-01       | 3                    | 2                         | 103.1 | En transit | null            | 2023-03-06 | Hamburg haffen | Berlin        |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/28.png)
___

### Affichez les entrepôts qui ont envoyé des expéditions à destination d'un entrepôt situé dans un pays différent.

On joint la table _entrepots_ ici pour permettre l'accès au nom des entrepots de départ et des déstination.

```sql
SELECT expeditions.*, src.nom_entrepot, des.nom_entrepot
FROM expeditions
         INNER JOIN transport_logistique.entrepots src on expeditions.id_entrepot_source = src.id
         INNER JOIN transport_logistique.entrepots des on expeditions.id_entrepot_destination = des.id
WHERE src.pays_id <> des.pays_id;
```

*Tableau de données de sortie de la requête SQL*

| id | date\_expedition | id\_entrepot\_source | id\_entrepot\_destination | poids   | statut     | date\_reception | date\_eta  | src.nom\_entrepot | des.nom\_entrepot |
|:---|:-----------------|:---------------------|:--------------------------|:--------|:-----------|:----------------|:-----------|:------------------|:------------------|
| 1  | 2023-01-01       | 1                    | 2                         | 23.1    | En transit | null            | 2023-01-06 | Marseille Prado   | Berlin            |
| 6  | 2023-07-01       | 1                    | 5                         | 223.25  | En transit | null            | 2023-07-06 | Marseille Prado   | London            |
| 4  | 2023-05-10       | 2                    | 4                         | 3213.42 | En transit | null            | 2023-05-15 | Berlin            | Rome              |
| 11 | 2022-11-02       | 2                    | 7                         | 3243.3  | Livrée     | 2022-11-10      | 2022-11-07 | Berlin            | Taiwan            |
| 5  | 2023-06-01       | 3                    | 4                         | 2003.21 | Livrée     | 2023-06-04      | 2023-06-06 | Hamburg haffen    | Rome              |
| 3  | 2023-05-01       | 4                    | 5                         | 1000.1  | Livrée     | 2023-05-10      | 2023-05-06 | Rome              | London            |
| 7  | 2023-10-01       | 4                    | 1                         | 3153.72 | En transit | null            | 2023-10-06 | Rome              | Marseille Prado   |
| 10 | 2023-11-02       | 4                    | 6                         | 5123.63 | En transit | null            | 2023-11-07 | Rome              | Pekin             |
| 15 | 2023-11-03       | 4                    | 5                         | 2423.5  | En transit | null            | 2023-11-08 | Rome              | London            |
| 16 | 2023-10-03       | 4                    | 5                         | 2423.5  | En transit | null            | 2023-10-08 | Rome              | London            |
| 17 | 2023-09-03       | 4                    | 5                         | 2423.5  | En transit | null            | 2023-09-08 | Rome              | London            |
| 18 | 2023-09-20       | 4                    | 5                         | 2423.5  | En transit | null            | 2023-09-25 | Rome              | London            |
| 8  | 2023-11-01       | 5                    | 2                         | 5123.63 | Livrée     | 2023-11-03      | 2023-11-06 | London            | Berlin            |
| 12 | 2022-11-12       | 5                    | 6                         | 3543.3  | En transit | 2022-11-14      | 2022-11-17 | London            | Pekin             |
| 14 | 2022-11-24       | 6                    | 4                         | 463.3   | Livrée     | 2022-12-24      | 2022-11-29 | Pekin             | Rome              |
| 9  | 2023-11-03       | 7                    | 1                         | 5123.63 | En transit | null            | 2023-11-08 | Taiwan            | Marseille Prado   |
| 13 | 2022-11-21       | 8                    | 3                         | 233.3   | Livrée     | 2022-12-10      | 2022-11-26 | Taipei            | Hamburg haffen    |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/29.png)
___

### Affichez les expéditions en transit qui ont été initiées par un entrepôt situé dans un pays dont le nom commence par la lettre "F" et qui pèsent plus de 500 kg.
On joint la table _pays_ et _entrepots_ pour récupérer la première lettre du pays dans lequel se trouve l'entrepôt de départ.
```sql
SELECT expeditions.*, src.nom_entrepot
FROM expeditions
         INNER JOIN transport_logistique.entrepots src on expeditions.id_entrepot_source = src.id
         INNER JOIN transport_logistique.pays psrc on src.pays_id = psrc.id
WHERE SUBSTRING(psrc.nom_pays, 1, 1) = 'F'
  AND poids > 500;
```

*Tableau de données de sortie de la requête SQL*

| id | date\_expedition | id\_entrepot\_source | id\_entrepot\_destination | poids | statut     | date\_reception | date\_eta  | nom\_entrepot   |
|:---|:-----------------|:---------------------|:--------------------------|:------|:-----------|:----------------|:-----------|:----------------|
| 55 | 2023-09-20       | 1                    | 5                         | 2000  | En transit | null            | 2023-09-25 | Marseille Prado |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/30.png)
___

### Affichez le nombre total d'expéditions pour chaque combinaison de pays d'origine et de destination.
On joint la table _entrepots_ pour récupérer le nom des entrepôts de départ et d'arrivé.

```sql
SELECT CONCAT(src.nom_entrepot, ' - ', des.nom_entrepot) as noms_entrepots, COUNT(expeditions.id) as nb_expedition
FROM expeditions
         INNER JOIN transport_logistique.entrepots src on expeditions.id_entrepot_source = src.id
         INNER JOIN transport_logistique.entrepots des on expeditions.id_entrepot_destination = des.id
GROUP BY CONCAT(src.nom_entrepot, ' - ', des.nom_entrepot);
```

*Tableau de données de sortie de la requête SQL*

| noms\_entrepots          | nb\_expedition |
|:-------------------------|:---------------|
| Berlin - Rome            | 1              |
| Berlin - Taiwan          | 1              |
| Hamburg haffen - Berlin  | 1              |
| Hamburg haffen - Rome    | 1              |
| London - Berlin          | 1              |
| London - Pekin           | 1              |
| Marseille Prado - Berlin | 1              |
| Marseille Prado - London | 2              |
| Pekin - Rome             | 1              |
| Rome - London            | 5              |
| Rome - Marseille Prado   | 1              |
| Rome - Pekin             | 1              |
| Taipei - Hamburg haffen  | 1              |
| Taiwan - Marseille Prado | 1              |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/31.png)
___

### Affichez les entrepôts qui ont envoyé des expéditions au cours des 30 derniers jours et dont le poids total des expéditions est supérieur à 1000 kg.

Création d'une view pour pré-calculer les données de poids sur la table. On calcule le poids total des expéditions sur
les 30 derniers jours à partir du jour d'exécution de la requête.

```sql
CREATE VIEW weight_exp_last_thirty_day AS
SELECT src.*, SUM(expeditions.poids) as total_weight
FROM expeditions
         INNER JOIN transport_logistique.entrepots src on expeditions.id_entrepot_source = src.id
WHERE date_expedition > DATE_ADD(NOW(), INTERVAL -30 DAY)
GROUP BY src.id;
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/32.png)

Utilisation de la view avec les données pré-calculées de poids des expéditions et le filtre du poids de 1000kg comme
limite.

```sql
SELECT *
FROM weight_exp_last_thirty_day
WHERE total_weight > 1000;
```

*Tableau de données de sortie de la requête SQL*

| id | nom\_entrepot | adresse                   | ville  | pays\_id | total\_weight   |
|:---|:--------------|:--------------------------|:-------|:---------|:----------------|
| 4  | Rome          | 30bis impasse des chèvres | Rome   | 107      | 7547.1298828125 |
| 5  | London        | 2 chemin des as           | London | 76       | 5123.6298828125 |
| 7  | Taiwan        | 10 tokyo city             | Tokyo  | 113      | 5123.6298828125 |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/33.png)
___

### Affichez les expéditions qui ont été livrées avec un retard de plus de 2 jours ouvrables.

Ici on utilite de la colonne ajoutée précédemment _date_eta_ dans la table _expeditions_.

Mais avant de pouvoir faire la requête, on doit créer une fonction qui permet de calculer le nombre de jours ouvrables
entre deux dates. Cette fonction prend en paramètre deux dates et retourne le nombre de jours ouvrables qui les sépare.
La requête de création de la fonction se trouve dans [functions.sql](functions.sql)

#### Création de la fonction <span style="color: red">#supplémentaire</span>

```sql
CREATE FUNCTION get_open_days_between(start_date DATE, end_date DATE) RETURNS INT
BEGIN
    DECLARE open_days INT;
    DECLARE current_test_date DATE;
    SET open_days = 0;
    SET current_test_date = start_date;

    WHILE current_test_date < end_date
        DO
            -- Vérifiez si le jour actuel n'est ni un samedi ni un dimanche
            IF DAYOFWEEK(current_test_date) NOT IN (1, 7) THEN
                SET open_days = open_days + 1;
            END IF;

            SET current_test_date = DATE_ADD(current_test_date, INTERVAL 1 DAY);
        END WHILE;

    RETURN open_days;
END;
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/34.png)

Maintenant que la fonction est créée on peut faire la requête qui vérifie le nombre de jours d'écart supérieur à 2 jours
ouvrables.

```sql
SELECT expeditions.date_eta,
       expeditions.date_reception,
       DATEDIFF(expeditions.date_reception, expeditions.date_eta)              as datetiff,
       get_open_days_between(expeditions.date_eta, expeditions.date_reception) as late_days
FROM expeditions
WHERE get_open_days_between(expeditions.date_eta, expeditions.date_reception) > 2;
```

*Tableau de données de sortie de la requête SQL*

| date\_eta  | date\_reception | datetiff | late\_days |
|:-----------|:----------------|:---------|:-----------|
| 2022-11-07 | 2022-11-10      | 3        | 3          |
| 2022-11-26 | 2022-12-10      | 14       | 10         |
| 2022-11-29 | 2022-12-24      | 25       | 19         |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/35.png)
___

### Affichez le nombre total d'expéditions pour chaque jour du mois en cours, trié par ordre décroissant.
Pour récupérer le premier jour du mois on utilise l'assemblage de fonction.
```sql
DATE(DATE_SUB(NOW(), INTERVAL DAYOFMONTH(NOW()) - 1 DAY))
```
La requête complète :
```sql
SELECT date_expedition, COUNT(id) as nb_exp_in_day
FROM expeditions
WHERE date_expedition >= DATE(DATE_SUB(NOW(), INTERVAL DAYOFMONTH(NOW()) - 1 DAY))
GROUP BY date_expedition
ORDER BY COUNT(id) DESC;
```

*Tableau de données de sortie de la requête SQL*

| date\_expedition | nb\_exp\_in\_day |
|:-----------------|:-----------------|
| 2023-11-03       | 2                |
| 2023-11-01       | 1                |
| 2023-11-02       | 1                |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/36.png)
___

## T-SQL

Toutes les commandes de cette partie se trouvent dans le fichier [t-sql-query.sql](t-sql-query.sql).

### Créez une vue qui affiche les informations suivantes pour chaque entrepôt : nom de l'entrepôt, adresse complète, nombre d'expéditions envoyées au cours des 30 derniers jours.

```sql
CREATE VIEW last_thirty_days_entrepot_export AS
SELECT nom_entrepot, CONCAT(adresse, ', ', ville, ', ', nom_pays) as adresse_complete, COUNT(*) as exp_sent
FROM expeditions
         INNER JOIN transport_logistique.entrepots src on expeditions.id_entrepot_source = src.id
         INNER JOIN pays ON pays_id = pays.id
WHERE date_expedition > DATE_ADD(NOW(), INTERVAL -30 DAY)
GROUP BY src.id;
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/37.png)

Pour accéder aux données de cette vue, on peut faire la requête suivante :

```sql
SELECT *
FROM last_thirty_days_entrepot_export;
```

*Tableau de données de sortie de la requête SQL*

| nom\_entrepot | adresse\_complete                       | exp\_sent |
|:--------------|:----------------------------------------|:----------|
| Rome          | 30bis impasse des chèvres, Rome, Iran   | 2         |
| London        | 2 chemin des as, London, United Kingdom | 1         |
| Taiwan        | 10 tokyo city, Tokyo, Japan             | 1         |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/38.png)
___

### Créez une procédure stockée qui prend en entrée l'ID d'un entrepôt et renvoie le nombre total d'expéditions envoyées par cet entrepôt au cours du dernier mois.

```sql
CREATE PROCEDURE last_month_exp_by_id_entrepot(id_entrepot INT)
BEGIN
    SELECT COUNT(*) as exp_sent
    FROM expeditions
             INNER JOIN transport_logistique.entrepots src on expeditions.id_entrepot_source = src.id
    WHERE date_expedition > DATE_ADD(NOW(), INTERVAL -30 DAY)
      AND src.id = id_entrepot
    GROUP BY src.id;
END;
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/39.png)

Pour accéder aux données de cette vue, on peut faire la requête suivante :

```sql
CALL last_month_exp_by_id_entrepot(7);
```

*Tableau de données de sortie de la requête SQL*

| exp\_sent |
|:----------|
| 1         |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/40.png)
___

### Créez une fonction qui prend en entrée une date et renvoie le nombre total d'expéditions livrées ce jour-là.

```sql
CREATE FUNCTION get_delivery_in_day(query_date DATE) RETURNS INT
BEGIN
    DECLARE nb_delivery INT;
    SET nb_delivery = 0;

    SELECT COUNT(*) into nb_delivery FROM expeditions WHERE date_expedition = query_date;

    RETURN nb_delivery;
END;
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/41.png)

Pour accéder aux données de cette vue, on peut faire la requête suivante :

```sql
SELECT get_delivery_in_day('2022-11-02')
```

*Tableau de données de sortie de la requête SQL*

| get\_delivery\_in\_day\('2022-11-02'\) |
|:---------------------------------------|
| 1                                      |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/42.png)
___

## Bonus

Toutes les commandes de cette partie se trouvent dans les
fichiers [bonus-query.sql](bonus/bonus-query.sql), [insert-query.sql](bonus/insert-query.sql), [select-query.sql](bonus/select-query.sql)
qui se situent dans le dossier bonus.

### Création de la table _clients_

On crée la table _clients_ avec une modification, ici encore on utilise l'ID du pays et non pas la colonne table dans le
bud de garder une uniformité des données.

```sql
CREATE TABLE clients
(
    id      INT PRIMARY KEY AUTO_INCREMENT,
    nom     VARCHAR(80)  NOT NULL,
    address VARCHAR(255) NOT NULL,
    ville   VARCHAR(100) NOT NULL,
    pays_id INT,
    FOREIGN KEY (pays_id) REFERENCES pays (id)
);
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/43.png)
___

### Création de la table de jointure _expeditions\_clients_

On crée la table _expeditions\_clients_ demandée dans le TP sans modifications.

```sql
CREATE TABLE expeditions_clients
(
    id_expedition INT NOT NULL,
    id_client     INT NOT NULL,
    CONSTRAINT FK_id_expedition FOREIGN KEY (id_expedition) REFERENCES expeditions (id),
    CONSTRAINT FK_id_client FOREIGN KEY (id_client) REFERENCES clients (id)
);
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/44.png)
___

### Modification de la table _expeditions\_clients

On modifie ici la table _expeditions\_clients_ pour ajouter deux colonnes qui permettront de répondre aux besoins des
questions qui suivent dans le TP.

```sql
ALTER TABLE expeditions_clients
    DROP FOREIGN KEY FK_id_client,
    DROP id_client,
    ADD id_client_in  INT NOT NULL,
    ADD id_client_out INT NOT NULL,
    ADD CONSTRAINT FK_id_client_in FOREIGN KEY (id_client_in) REFERENCES clients (id),
    ADD CONSTRAINT FK_id_client_out FOREIGN KEY (id_client_out) REFERENCES clients (id);
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/45.png)
___

### Modification de la table expeditions <span style="color: red">#supplémentaire</span>

Il est demandé dans le TP de modifier la table client pour ajouter une colonne _id\_client_. Ci-après les requêtes
pour créer et retirer cette colonne. Pour des raisons de synchronisation des données, et de maintien de la cohérence
entre les clients et les expéditions, il est préférable de n'avoir qu'une table de jointure qui fait la liaison entre la
table _clients_ et _expeditions_. Il suffit alors d'ajouter, si besoin, un **INNER JOIN** dans la requête qui souhaite
faire la liaison des tables.

Requête d'ajout :

```sql
ALTER TABLE expeditions
    ADD id_client INT,
    ADD CONSTRAINT FK_id_client_clients_id FOREIGN KEY (id_client) REFERENCES clients (id);
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/46.png)
___

Requête de suppréssion :

```sql
ALTER TABLE expeditions
    DROP FOREIGN KEY FK_id_client_clients_id,
    DROP id_client;
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/47.png)
___

### Ajout de données dans la table _clients_

On insert des données dans la table _clients_ en tenant compte des modifications par rapport à la colonne _pays\_id_.

```sql
INSERT INTO clients(nom, address, ville, pays_id)
VALUES ('Florian', '4 rue Paul Montrochet', 'Lyon', 74),
       ('Ji', '29 drift road', 'Tokyo', 113),
       ('Frank', '2 Albert Strasse', 'Frankfort', 56),
       ('Matt', '22 jump street', 'New York', 230),
       ('Shi', '16 china street', 'Pekin', 48),
       ('Corentin', '1342 rue de la Concorde', 'Paris', 74);
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/48.png)
___

### Ajout de données dans la table _expeditions\_clients_

On insert des données dans la table _expeditions\_clients_ comme elle est identique à la demande du TP il n'y pas de
point particulier lors de la création des données.

```sql
INSERT INTO expeditions_clients (id_expedition, id_client_in, id_client_out)
VALUES (1, 2, 3)
     , (2, 4, 2)
     , (3, 4, 2)
     , (4, 5, 3)
     , (5, 2, 4)
     , (6, 5, 6)
     , (7, 3, 1)
     , (8, 2, 3)
     , (9, 5, 2)
     , (10, 1, 5)
     , (11, 1, 3)
     , (12, 1, 5)
     , (13, 2, 5)
     , (14, 2, 4)
     , (15, 5, 3)
     , (16, 5, 1)
     , (17, 1, 4)
     , (18, 1, 6);
```

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/49.png)
___

### Requêtes

Toutes les commandes de cette partie se trouvent dans le
fichier [select-query.sql](bonus/select-query.sql) qui se situent dans le dossier bonus.

#### Pour chaque client, affichez son nom, son adresse complète, le nombre total d'expéditions qu'il a envoyées et le nombre total d'expéditions qu'il a reçues.

```sql
SELECT nom,
       CONCAT(address, ', ', ville, ', ', nom_pays)                                                    as adresse_complete,
       (SELECT COUNT(*) FROM expeditions_clients WHERE expeditions_clients.id_client_out = clients.id) as nb_exp_sent,
       (SELECT COUNT(*)
        FROM expeditions_clients
        WHERE expeditions_clients.id_client_in = clients.id)                                           as nb_exp_received
FROM clients
         INNER JOIN pays ON pays_id = pays.id;
```

*Tableau de données de sortie de la requête SQL*

| nom      | adresse\_complete                                  | nb\_exp\_sent | nb\_exp\_received |
|:---------|:---------------------------------------------------|:--------------|:------------------|
| Florian  | 4 rue Paul Montrochet, Lyon, France                | 2             | 5                 |
| Ji       | 29 drift road, Tokyo, Japan                        | 3             | 5                 |
| Frank    | 2 Albert Strasse, Frankfort, Germany               | 5             | 1                 |
| Matt     | 22 jump street, New York, United States of America | 3             | 2                 |
| Shi      | 16 china street, Pekin, China                      | 3             | 5                 |
| Corentin | 1342 rue de la Concorde, Paris, France             | 2             | 0                 |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/50.png)
___

#### Pour chaque expédition, affichez son ID, son poids, le nom du client qui l'a envoyée, le nom du client qui l'a reçue et le statut

```sql
SELECT expeditions.id, poids, cout.nom as client_emiter, cin.nom as client_receiver, statut
FROM expeditions
         INNER JOIN transport_logistique.expeditions_clients ec on expeditions.id = ec.id_expedition
         INNER JOIN transport_logistique.clients cin on ec.id_client_in = cin.id
         INNER JOIN transport_logistique.clients cout on ec.id_client_out = cout.id;
```

*Tableau de données de sortie de la requête SQL*

| id | poids   | client\_emiter | client\_receiver | statut     |
|:---|:--------|:---------------|:-----------------|:-----------|
| 7  | 3153.72 | Florian        | Frank            | En transit |
| 16 | 2423.5  | Florian        | Shi              | En transit |
| 2  | 103.1   | Ji             | Matt             | En transit |
| 3  | 1000.1  | Ji             | Matt             | Livrée     |
| 9  | 5123.63 | Ji             | Shi              | En transit |
| 1  | 23.1    | Frank          | Ji               | En transit |
| 4  | 3213.42 | Frank          | Shi              | En transit |
| 8  | 5123.63 | Frank          | Ji               | Livrée     |
| 11 | 3243.3  | Frank          | Florian          | Livrée     |
| 15 | 2423.5  | Frank          | Shi              | En transit |
| 5  | 2003.21 | Matt           | Ji               | Livrée     |
| 14 | 463.3   | Matt           | Ji               | Livrée     |
| 17 | 2423.5  | Matt           | Florian          | En transit |
| 10 | 5123.63 | Shi            | Florian          | En transit |
| 12 | 3543.3  | Shi            | Florian          | En transit |
| 13 | 233.3   | Shi            | Ji               | Livrée     |
| 6  | 223.25  | Corentin       | Shi              | En transit |
| 18 | 2000    | Corentin       | Florian          | En transit |

*Capture d'écran du terminal de sortie de la requête SQL*

![](images/51.png)
___
