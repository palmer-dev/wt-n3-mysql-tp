-- PRECISION DE LA BASE DE DONNÉES À UTILISER
USE transport_logistique;

-- AFFICHER LE NOM, L'ADRESSE COMPLETTE, LE NOMBRE D'EXPÉDITIONS ENVOYÉE ET RECUES DE CHAQUE CLIENT
SELECT nom,
       CONCAT(address, ', ', ville, ', ', nom_pays)                                                    as adresse_complete,
       (SELECT COUNT(*) FROM expeditions_clients WHERE expeditions_clients.id_client_out = clients.id) as nb_exp_sent,
       (SELECT COUNT(*)
        FROM expeditions_clients
        WHERE expeditions_clients.id_client_in = clients.id)                                           as nb_exp_received
FROM clients
         INNER JOIN pays ON pays_id = pays.id;

-- POUR CHAQUE EXPÉDITION, AFFICHEZ SON ID, SON POIDS, LE NOM DU CLIENT QUI L'A ENVOYÉE, LE NOM DU CLIENT QUI L'A REÇUE ET LE STATUT
SELECT expeditions.id, poids, cout.nom as client_emiter, cin.nom as client_receiver, statut
FROM expeditions
         INNER JOIN transport_logistique.expeditions_clients ec on expeditions.id = ec.id_expedition
         INNER JOIN transport_logistique.clients cin on ec.id_client_in = cin.id
         INNER JOIN transport_logistique.clients cout on ec.id_client_out = cout.id;
