-- PRECISION DE LA BASE DE DONNÉES À UTILISER
USE transport_logistique;

-- CRÉATION DE LA TABLE clients
CREATE TABLE clients
(
    id      INT PRIMARY KEY AUTO_INCREMENT,
    nom     VARCHAR(80)  NOT NULL,
    address VARCHAR(255) NOT NULL,
    ville   VARCHAR(100) NOT NULL,
    pays_id INT,
    FOREIGN KEY (pays_id) REFERENCES pays (id)
);


-- CRÉATION DE LA TABLE DE JOINTURE expeditions_clients
CREATE TABLE expeditions_clients
(
    id_expedition INT NOT NULL,
    id_client     INT NOT NULL,
    CONSTRAINT FK_id_expedition FOREIGN KEY (id_expedition) REFERENCES expeditions (id),
    CONSTRAINT FK_id_client FOREIGN KEY (id_client) REFERENCES clients (id)
);

-- AJOUT DE LA COLONNE id_client_out et id_client_in ET SUPPRESSION DE LA COLONNE id_client
-- id_client_out représente le client qui envoie l'expedition & id_client_in représente le client qui reçoit l'expedition
ALTER TABLE expeditions_clients
    DROP FOREIGN KEY FK_id_client,
    DROP id_client,
    ADD id_client_in  INT NOT NULL,
    ADD id_client_out INT NOT NULL,
    ADD CONSTRAINT FK_id_client_in FOREIGN KEY (id_client_in) REFERENCES clients (id),
    ADD CONSTRAINT FK_id_client_out FOREIGN KEY (id_client_out) REFERENCES clients (id);

-- AJOUT DE LA COLONNE id_client DANS LA TABLE expeditions
ALTER TABLE expeditions
    ADD id_client INT,
    ADD CONSTRAINT FK_id_client_clients_id FOREIGN KEY (id_client) REFERENCES clients (id);

-- RETRAIT DE LA COLONNE id_client DANS LA TABLE expeditions
ALTER TABLE expeditions
    DROP FOREIGN KEY FK_id_client_clients_id,
    DROP id_client;
