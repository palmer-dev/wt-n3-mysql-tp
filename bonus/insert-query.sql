-- PRECISION DE LA BASE DE DONNÉES À UTILISER
USE transport_logistique;

-- AJOUT DE DONNÉES À LA TABLE clients
INSERT INTO clients(nom, address, ville, pays_id)
VALUES ('Florian', '4 rue Paul Montrochet', 'Lyon', 74),
       ('Ji', '29 drift road', 'Tokyo', 113),
       ('Frank', '2 Albert Strasse', 'Frankfort', 56),
       ('Matt', '22 jump street', 'New York', 230),
       ('Shi', '16 china street', 'Pekin', 48),
       ('Corentin', '1342 rue de la Concorde', 'Paris', 74);


-- AJOUT DE DONNÉES À LA TABLE expeditions_clients
INSERT INTO expeditions_clients (id_expedition, id_client_in, id_client_out)
VALUES (1, 2, 3)
     , (2, 4, 2)
     , (3, 4, 2)
     , (4, 5, 3)
     , (5, 2, 4)
     , (6, 5, 6)
     , (7, 3, 1)
     , (8, 2, 3)
     , (9, 5, 2)
     , (10, 1, 5)
     , (11, 1, 3)
     , (12, 1, 5)
     , (13, 2, 5)
     , (14, 2, 4)
     , (15, 5, 3)
     , (16, 5, 1)
     , (17, 1, 4)
     , (18, 1, 6);
