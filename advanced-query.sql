-- PRECISION DE LA BASE DE DONNÉES À UTILISER
USE transport_logistique;

-- AFFICHEZ LES ENTREPÔTS QUI ONT ENVOYÉ AU MOINS UNE EXPÉDITION EN TRANSIT.
SELECT entrepots.*
FROM entrepots
         INNER JOIN transport_logistique.expeditions e on entrepots.id = e.id_entrepot_source
WHERE statut = 'En transit'
GROUP BY entrepots.id;

-- AFFICHEZ LES ENTREPÔTS QUI ONT REÇU AU MOINS UNE EXPÉDITION EN TRANSIT.
SELECT entrepots.*
FROM entrepots
         INNER JOIN transport_logistique.expeditions e on entrepots.id = e.id_entrepot_destination
WHERE statut = 'En transit'
GROUP BY entrepots.id;

-- AFFICHEZ LES EXPÉDITIONS QUI ONT UN POIDS SUPÉRIEUR À 100 KG ET QUI SONT EN TRANSIT.
SELECT entrepots.*
FROM entrepots
         INNER JOIN transport_logistique.expeditions e on entrepots.id = e.id_entrepot_destination
WHERE statut = 'En transit'
  AND poids > 100
GROUP BY entrepots.id;

-- AFFICHEZ LE NOMBRE D'EXPÉDITIONS ENVOYÉES PAR CHAQUE ENTREPÔT.
SELECT entrepots.*, COUNT(*) as nb_expedition
FROM entrepots
         INNER JOIN transport_logistique.expeditions e on entrepots.id = e.id_entrepot_source
GROUP BY entrepots.id;

-- AFFICHEZ LE NOMBRE TOTAL D'EXPÉDITIONS EN TRANSIT.
SELECT COUNT(*) as nb_expeditions_transit
FROM expeditions
WHERE statut = 'En transit';

-- AFFICHEZ LE NOMBRE TOTAL D'EXPÉDITIONS LIVRÉES.
SELECT COUNT(*) as nb_expeditions_livrees
FROM expeditions
WHERE statut = 'Livrée';

-- AFFICHEZ LE NOMBRE TOTAL D'EXPÉDITIONS POUR CHAQUE MOIS DE L'ANNÉE EN COURS.
SELECT MONTH(date_expedition) as month_expedition, COUNT(*) as nb_expeditions
FROM expeditions
WHERE YEAR(date_expedition) = YEAR(NOW())
GROUP BY MONTH(date_expedition);

-- AFFICHEZ LES ENTREPÔTS QUI ONT ENVOYÉ DES EXPÉDITIONS AU COURS DES 30 DERNIERS JOURS.
SELECT etr.*
FROM expeditions
         INNER JOIN transport_logistique.entrepots etr on expeditions.id_entrepot_source = etr.id
WHERE DATEDIFF(NOW(), date_expedition) < 30
GROUP BY etr.id;

-- AFFICHEZ LES ENTREPÔTS QUI ONT REÇU DES EXPÉDITIONS AU COURS DES 30 DERNIERS JOURS.
SELECT etr.*
FROM expeditions
         INNER JOIN transport_logistique.entrepots etr on expeditions.id_entrepot_source = etr.id
WHERE DATEDIFF(NOW(), date_reception) < 30
GROUP BY etr.id;

-- AFFICHEZ LES EXPÉDITIONS QUI ONT ÉTÉ LIVRÉES DANS UN DÉLAI DE MOINS DE 5 JOURS OUVRABLES.
SELECT expeditions.*
FROM expeditions
WHERE DATEDIFF(date_reception, date_expedition) < 5;
